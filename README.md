Server - REST API application

Реализованы роли доступа:

* P - Public - общедоступный
* U - User - зарегистрированный пользователь
* A - Admin - администратор

User включает в себя Public
Admin включает в себя User и Public

В JSON запросе к ссылке, в которой указаны роли доступа User или Admin, требуется указать токен, полученный при авторизации:


```
#!JSON

{  "token": "header.payLoad.signature" }
```

### Стандартные ответы во всех запросах к БД ###

**Отсутствие подключения к БД у сервера**

Сервер отдает такую ошибку, когда при запросе не может выполнить подключение к БД. 

```
#!JSON
{
  "code": "NOT_CONNECTION_TO_DB",
  "status": 500,
  "message": "not connection to db"
}
```

**"Плохой запрос"**

Ошибка возникает, когда БД выдает ошибку при обработке SQL запроса с заданными параметрами. 
```
#!JSON
{
  "code": "BAD_QUERY",
  "status": 400,
  "message": "bad query"
}
```

**%SOMEBODY% не найдено**

Ошибка возникает, если БД возвращает undefined. Также возвращается PAGE_NOT_FOUND при запросе к неопределенным ссылкам.

```
#!JSON
{
  "code": "%SOMEBODY%_NOT_FOUND",
  "status": 404,
  "message": "%somebody% not found",
}
```

**Internal server error**

Ошибка возникает, когда происходит какая-то внутренняя ошибка, которая обработана только конечным обработчиком ошибок. Означает, что это какая то недоработка. 

```
#!JSON
{
  "code": "INTERNAL_SERVER_ERROR",
  "status": 500,
  "message": "internal server error",
  "attr": "Ответ который отдает логгер сервера"
}
```

# Запросы: #

[Access Role]/[Type of Request] : [Link]

[Роль доступа]/[Тип запроса] : [Ссылка]

## Запросы по странам, городам, регионам ##

### P/GET: '/api/countries' ###
Получение списка всех стран 

**Ответ:**

```
#!JSON
[
  {
    "country_id": 1,
    "country_name": "Россия",
    "country_name_en": "Russia"
  },
  {
    "country_id": 2,
    "country_name": "Украина",
    "country_name_en": "Ukraine"
  }, ...
]
```

### P/GET: '/api/regions/?country_id' ###
Получение списка регионов страны country_id 

**Ответ:**
```
#!JSON
[
  {
    "region_id": 1,
    "region_name": "Алтай",
    "country_id": 1
  }, ...
]

```

###P/GET: '/api/cities/?country_id'###
Получение списка городов страны

**Ответ:**

```
#!JSON
[
  {
    "city_id": 1,
    "city_name": "Москва"
  }, ...
]

```

###P/GET: '/api/cities/?region_id' ###
Получение списка городов региона

**Ответ:**

```
#!JSON
[
  {
    "city_id": 1,
    "city_name": "Москва"
  }, ...
]

```
###P/GET: '/api/countries/:country_id' ###
Получение информации по стране

**Ответ:**

```
#!JSON
{
  "country_id": 1,
  "country_name": "Россия",
  "country_name_en": "Russia"
}

```

###P/GET: '/api/regions/:region_id' ###
Получение информации по региону

**Ответ:**

```
#!JSON
{
  "region_id": 85,
  "region_name": "Санкт-Петербург город",
  "country_id": 1
}

```
###P/GET: '/api/cities/:city_id'###

Получение информации по городу

**Ответ:**

```
#!JSON
{
  "city_id": 2,
  "city_name": "Санкт-Петербург"
}
```

## Запросы по маршрутам ##

###P/GET: '/api/routes/:route_id'###

Получение информации по маршруту

**Ответ:**

```
#!JSON
[
  {
    "route_id": 1,
    "typeofmovement_id": 3,
    "route_approved": true,
    "city_id": 34,
    "route_description": "Description",
    "user_id": 25,
    "route_name": "Routes"
  },
  [
    {
      "theme_id": 1,
      "route_id": 1
    },
    {
      "theme_id": 2,
      "route_id": 1
    }
  ]
]
```

###U/POST: '/api/routes/'###

Добавление маршрута


**JSON:**

```
#!JSON
{
  "route_name" : "string"
  "user_id" : int,
  "city_id" : int,
  "route_description" : "string",
  "typeofmovement_id" : int
}
```

**Ответ в случае успеха:**

```
#!JSON
{
  "code": "INSERT_ROUTE",
  "status": 200,
  "message": "insert route"
}
```

**Ответ в случае провала:**

```
#!JSON
{
  "code": "BAD_QUERY",
  "status": 400,
  "message": "bad query",
  "attr": {
    "name": "error",
    "length": 183,
    "severity": "ERROR",
    "code": "23505",
    "detail": "Key (route_id)=(15) already exists.",
    "schema": "public",
    "table": "routes",
    "constraint": "routes_pkey",
    "file": "nbtinsert.c",
    "line": "406",
    "routine": "_bt_check_unique"
}
```
###U/PUT: '/api/routes/:route_id'###
Обновление информации по маршруту

**JSON:**

```
#!JSON
{
  "route_name" : "string"
  "city_id" : int,
  "route_description" : "string",
  "typeofmovement_id" : int
}
```

**Ответ в случае успеха:**

```
#!JSON
{
  "code": "UPDATE_ROUTE",
  "status": 200,
  "message": "update route"
}
```

**Ответ в случае провала:**

```
#!JSON
{
  "code": "BAD_QUERY",
  "status": 400,
  "message": "bad query",
  "attr": {
    "name": "error",
    "length": 183,
    "severity": "ERROR",
    "code": "23505",
    "detail": "Key (route_id)=(15) already exists.",
    "schema": "public",
    "table": "routes",
    "constraint": "routes_pkey",
    "file": "nbtinsert.c",
    "line": "406",
    "routine": "_bt_check_unique"
}
```
**или если не найден:**

```
#!JSON
{
  "code": "ROUTE_NOT_FOUND",
  "status": 404,
  "message": "route not found"
}
``` 
###U/DELETE: '/api/routes/:route_id'###

Удаление маршрута 

**Ответ в случае успеха:**

```
#!JSON
{
  "code": "UPDATE_ROUTE",
  "status": 200,
  "message": "update route"
}
```

**Ответ в случае провала:**

```
#!JSON
{
  "code": "BAD_QUERY",
  "status": 400,
  "message": "bad query",
  "attr": {
    "name": "error",
    "length": 183,
    "severity": "ERROR",
    "code": "23505",
    "detail": "Key (route_id)=(15) already exists.",
    "schema": "public",
    "table": "routes",
    "constraint": "routes_pkey",
    "file": "nbtinsert.c",
    "line": "406",
    "routine": "_bt_check_unique"
}
```
**или если не найден:**

```
#!JSON
{
  "code": "ROUTE_NOT_FOUND",
  "status": 404,
  "message": "route not found"
}
``` 

## Запросы по подмаршрутам ##

###P/GET: '/api/routeparts/:route_id'###
Получение всех подмаршрутов по маршруту

**Ответ:**

```
#!JSON
[
  {
    "route_id": 1,
    "typeofmovement_id": 1,
    "route_approved": true,
    "city_id": 32,
    "route_description": "dsd",
    "user_id": 2,
    "route_name": "vdsvds"
  },
  [
    {
      "theme_id": 1,
      "route_id": 1
    },
    {
      "theme_id": 2,
      "route_id": 1
    }
  ]
]
```

###U/POST: '/api/routeparts/'###

Добавление подмаршрутов. Можно отправлять сразу же всем массивом подмаршрутов.

**JSON:**

```
#!JSON
"array_parts": [
    {
      "rp_st_lat": int,
      "rp_st_lon": int,
      "rp_en_lat": int,
      "rp_en_lon": int,
      "user_id": int,
      "route_id": int,
      "part_id": int
    }, ...
]
```

**Ответ в случае успеха:**

```
#!JSON
{
  "code": "INSERT_ROUTEPARTS",
  "status": 200,
  "message": "insert routeparts"
}
```

**Ответ в случае провала:**

```
#!JSON
{
  {
  "code": "BAD_QUERY",
  "status": 400,
  "message": "bad query",
  "attr": [
    {
      "name": "error",
      "length": 85,
      "severity": "ERROR",
      "code": "42601",
      "position": "145",
      "file": "scan.l",
      "line": "1053",
      "routine": "scanner_yyerror"
    },
    [
      1, // Это те parts, которые были вставлены в таблицу
      2,
      3
    ]
  ]
}
}
```

* U/PUT: '/api/routeparts/:route_id' - Обновление информации по подмаршрутам маршрута 
* U/DELETE: '/api/routeparts/:routepart_id' - Удаление подмаршрута

## Запросы по темам ##
* P/GET: '/api/themes/:theme_id' - Получение информации по теме
* P/GET: '/api/themes/:theme_id/list' - Получение списка всех маршрутов с этой темой

## Запросы по достопримечательностям ##
* P/GET: '/api/sights/:sight_id' - Получение информации по достопримечательностям

## Запросы по типу передвижения ##
* P/GET: /api/typesofmovement/:typeofmovement_id

## Запрос по авторизации ##
* P/POST: '/api/login'