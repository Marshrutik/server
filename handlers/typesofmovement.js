var _error = require('../libs/error');
var db = require('../libs/db');

module.exports.get = function(req, res) {
    db.getInfoAboutTypesOfMovement(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};