var _error = require('../libs/error');
var db = require('../libs/db');

//Get all information about country with country_id
module.exports.get = function(req, res) {
    db.getInfoAboutCountry(req, function (err, data) {
        if (err)
            _error.send(res, err);
        else
            res.send(data);
    })
};

//Get information about all countries
module.exports.list = function(req, res) {
   db.getInfoAboutAllCountries(req, function(err, data) {
       if (err)
            _error.send(res, err);
       else
            res.send(data);
   })
};