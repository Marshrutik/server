var _error = require('../libs/error');
var db = require('../libs/db');

//Get all information about region with region_id
module.exports.get = function(req, res) {
   db.getInfoAboutRegion(req, function(err, data){
       if(err)
            _error.send(res, err);
       else
            res.send(data);
   })
};

//Get list of region in country with country_id
module.exports.list = function(req, res) {
    db.getInfoAboutAllRegions(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    });
};