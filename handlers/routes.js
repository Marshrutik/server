var _error = require('../libs/error');
var _ok = require('../libs/ok');
var db = require('../libs/db');


module.exports.get = function(req, res) {
    db.getInfoAboutRoute(req, function(err, data) {
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};

module.exports.getAll = function(req, res) {
    db.getAllInfoAboutRoute(req, function(err, data) {
        if (err)
            _error.send(res, err);
        else
            res.send(data);
    })
};

module.exports.post = function(req, res) {
    db.insertIntoAboutRoute(req, function(err, info){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.INSERT_ROUTE, info);
    })
};

module.exports.delete = function(req, res) {
    db.deleteInfoAboutRoute(req, function(err, info){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.DELETE_ROUTE);
    })
};

module.exports.put = function(req, res) {
    db.updateInfoAboutRoute(req, function(err, info){
        if(err)
            _error.send(res,err);
        else
            _ok.send(res, _ok.UPDATE_ROUTE);
    })
};

module.exports.getInfoAboutRouteOfUser = function (req, res) {
    db.getInfoAboutRouteOfUser(req, function(err, info){
        if(err)
            _error.send(res, err);
        else
            res.send(info);
    })
};


