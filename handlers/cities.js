var _error = require('../libs/error');
var db = require('../libs/db');

//Get all information about city with city_id
module.exports.get = function(req, res) {
    db.getInfoAboutCity(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};
//Get list of city in country/ country and region
module.exports.list = function(req, res) {
    if(req.query.country_id){
        db.getInfoAboutCityFromCountry(req, function(err, data){
            if(err)
                _error.send(res, err);
            else
                res.send(data);
        })
    }
    else if (req.query.region_id){
        db.getInfoAboutCityFromRegion(req, function(err, data){
            if(err)
                _error.send(res, err);
            else
                res.send(data);
        })
    }
    else
        _error.send(res, _error.BAD_QUERY);
};

module.exports.getRoutes = function(req, res) {
    db.getInfoAboutRoutesInCity(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};


module.exports.getSights = function(req, res) {
    db.getInfoAboutSightInCity(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
}
