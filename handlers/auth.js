var _error = require('../libs/error');
var _ok = require('../libs/ok');
var db = require('../libs/db');
var security = require('../libs/security');

module.exports.login = function(req, res) {
    db.getInfoAboutUsername(req, function(err, infoUser){
        if(err)
            _error.send(res, err);
        else {
            security.getToken(infoUser, function(err, token){
                if(err)
                    _error.send(res, err);
                else
                    _ok.send(res, _ok.LOGIN_SUCCESS, {token: token});
            })
        }
    });
};

module.exports.signup = function (req, res) {
    db.insertNewUser(req, function(err, info){
        if(err)
            _error.send(res, err);
        else {
            console.log(req.body);
            security.getToken(req.body, function(err, token){
                if (err)
                    _error.send(res, err);
                else {
                    info.token = token;
                    _ok.send(res, _ok.INSERT_USER, info);
                }
            })
        }
    })
};



