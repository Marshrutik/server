var _error = require('../libs/error');
var _ok = require('../libs/ok');
var db = require('../libs/db');

module.exports.get = function(req, res) {
    db.getInfoAboutRoutePart(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};


module.exports.post = function(req, res) {
    db.insertInfoAboutAllRouteParts(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.INSERT_ROUTEPARTS, data);
    })
};

module.exports.postOne = function(req, res) {
    db.insertInfoAboutRoutePart(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.INSERT_ROUTEPARTS, data);
    })
};

module.exports.delete = function(req, res) {
    db.deleteInfoAboutRoutePart(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.DELETE_ROUTEPARTS);
    })
};
module.exports.put = function(req, res) {
    db.updateInfoAboutRoutePart(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.UPDATE_ROUTEPARTS);
    });
};

module.exports.updateInfoAllRouteParts = function (req, res) {
    db.updateInfoAllRouteParts(req, function(err, info){
        if(err)
            _error.send(res, err);
        else
            _ok.send(res, _ok.UPDATE_ROUTEPARTS);
    })
};
