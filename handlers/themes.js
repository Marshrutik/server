var _error = require('../libs/error');
var db = require('../libs/db');

module.exports.get = function(req, res) {
    db.getInfoAboutTheme(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};

module.exports.list = function(req, res) {
    db.getInfoAboutRouteInThemes(req, function(err, data){
        if(err)
            _error.send(res, err);
        else
            res.send(data);
    })
};