var express = require('express'); //Подключение модуля Express
var log = require('./log'); // Подключение модуля логгирования
var http = require('http'); // Подключение модуля сервера

app = express();

require('./routes')(app); // Подключения маршрутизатора запроса
require('./boot')(app); // Подключение настроенные модулей

http.createServer(app).listen(app.get('port'), function () {
    log.info("Server running on port:" + app.get('port'));
});


