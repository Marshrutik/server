var secret = require('../libs/config/secret');
var jwt = require('jwt-simple');
var db = require('./db');

module.exports.getToken = function (data, callback) {
    var expires = expiresIn(365); // 365 days
    data.user_token = jwt.encode({
        exp: expires,
        username: data.user_username,
        firstname: data.user_firstname,
        lastname: data.user_lastname
    }, secret());

    db.updateToken(data, function(err){
        if (err)
            callback(err);
        else
            callback(null, data.user_token);
    });
    return data.user_token
};

module.exports.decodeToken = function (token) {
    var decoded = jwt.decode(token, secret());
    return {
        "user_username"  : decoded.username,
        "user_firstname" : decoded.firstname,
        "user_lastname"  : decoded.lastname
    };
};

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}