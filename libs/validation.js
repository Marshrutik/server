function Validation (req){
    this.req = req;
    this.data = {};
};

Validation.prototype.checkData = function() {
    for (obj in this.data) {
        if (!this.data[obj])
            return false;
    }
    return true;
};

Validation.prototype.getData = function() {
    return this.data;
};

Validation.prototype.params_country_id = function() {
    if(this.req.params.country_id)
        return this.data.country_id = Number(this.req.params.country_id);
    else
        return this.data.country_id = false;
};

Validation.prototype.params_city_id = function() {
    if(this.req.params.city_id)
        return this.data.city_id = Number(this.req.params.city_id);
    else
        return this.data.city_id = false;
};

Validation.prototype.params_region_id = function() {
    if(this.req.params.region_id)
        return this.data.region_id = Number(this.req.params.region_id);
    else
        return this.data.region_id = false;
};

Validation.prototype.query_region_id = function() {
    if(this.req.query.region_id)
        return this.data.region_id = Number(this.req.query.region_id);
    else
        return this.data.region_id = false;
};
Validation.prototype.query_country_id = function() {
    if(this.req.query.country_id)
        return this.data.country_id = Number(this.req.query.country_id);
    else
        return this.data.country_id = false;
};
Validation.prototype.params_routepart_id = function() {
    if(this.req.params.routepart_id)
        return this.data.routepart_id = Number(this.req.params.routepart_id);
    else
        return this.data.routepart_id = false;
};
Validation.prototype.params_route_id = function() {
    if(this.req.params.route_id)
        return this.data.route_id = Number(this.req.params.route_id);
    else
        return this.data.route_id = false;
};

Validation.prototype.body_route_name = function() {
    if(this.req.body.route_name)
        return this.data.route_name = String(this.req.body.route_name);
    else
        return this.data.route_name = false;
};
Validation.prototype.user_user_id = function() {
    if(this.req.user.user_id)
        return this.data.user_id = this.req.user.user_id;
    else
        return this.data.user_id = false;
};
Validation.prototype.body_city_id = function() {
    if (this.req.body.city_id)
        return this.data.city_id = Number(this.req.body.city_id);
    else
        return this.data.city_id = false;
};
Validation.prototype.body_route_description = function() {
    if (this.req.body.route_description)
        return this.data.route_description = String(this.req.body.route_description);
    else
        return this.data.route_description = false;
};

Validation.prototype.body_typeofmovement_id = function() {
    if (this.req.body.typeofmovement_id)
        return this.data.typeofmovement_id = Number(this.req.body.typeofmovement_id);
    else
        return this.data.typeofmovement_id = false;
};
Validation.prototype.body_routepart_id = function() {
    if (this.req.body.routepart_id)
        return this.data.routepart_id = Number(this.req.body.routepart_id);
    else
        return this.data.routepart_id = false;
};

Validation.prototype.body_route_id = function() {
    if (this.req.body.route_id)
        return this.data.route_id = Number(this.req.body.route_id);
    else
        return this.data.route_id = false;
};
Validation.prototype.user_user_role = function() {
    if (this.req.user.user_role)
        return this.data.user_role = String(this.req.user.user_role);
    else
        return this.data.user_role = false;
};
Validation.prototype.user_user_token = function() {
    if (this.req.user.user_token)
        return this.data.user_token = String(this.req.user.user_token);
    else
        return this.data.user_token = false;
};
Validation.prototype.body_user_token = function() {
    if (this.req.body.user_token)
        return this.data.user_token = String(this.req.body.user_token);
    else
        return this.data.user_token = false;
};

Validation.prototype.params_sight_id = function() {
    if(this.req.params.sight_id)
        return this.data.sight_id = Number(this.req.params.sight_id);
    else
        return this.data.sight_id = false;
};

Validation.prototype.params_theme_id = function() {
    if(this.req.params.theme_id)
        return this.data.theme_id = Number(this.req.params.theme_id);
    else
        return this.data.theme_id = false;
};

Validation.prototype.params_typeofmovement_id = function() {
    if(this.req.params.typeofmovement_id)
        return this.data.typeofmovement_id = Number(this.req.params.typeofmovement_id);
    else
        return this.data.typeofmovement_id = false;
};

Validation.prototype.body_routepart_start_latitude = function() {
    if (this.req.body.routepart_start_latitude)
        return this.data.routepart_start_latitude = String(this.req.body.routepart_start_latitude);
    else
        return this.data.routepart_start_latitude = false;
};

Validation.prototype.body_routepart_start_longitude = function() {
    if (this.req.body.routepart_start_longitude)
        return this.data.routepart_start_longitude = String(this.req.body.routepart_start_longitude);
    else
        return this.data.routepart_start_longitude = false;
};

Validation.prototype.body_routepart_end_latitude = function() {
    if (this.req.body.routepart_end_latitude)
        return this.data.routepart_end_latitude = String(this.req.body.routepart_end_latitude);
    else
        return this.data.routepart_end_latitude = false;
};

Validation.prototype.body_routepart_end_longitude = function() {
    if (this.req.body.routepart_end_longitude)
        return this.data.routepart_end_longitude = String(this.req.body.routepart_end_longitude);
    else
        return this.data.routepart_end_longitude = false;
};

Validation.prototype.body_part_id = function() {
    if (this.req.body.part_id)
        return this.data.part_id = Number(this.req.body.part_id);
    else
        return this.data.part_id = false;
};

Validation.prototype.body_user_username = function() {
    if (this.req.body.user_username)
        return this.data.user_username = String(this.req.body.user_username);
    else
        return this.data.user_username = false;
};


Validation.prototype.body_user_password = function() {
    if (this.req.body.user_password)
        return this.data.user_password = String(this.req.body.user_password);
    else
        return this.data.user_password = false;
};

Validation.prototype.body_user_email = function() {
    if (this.req.body.user_email)
        return this.data.user_email = String(this.req.body.user_email);
    else
        return this.data.user_email = false;
};


Validation.prototype.body_user_firstname = function() {
    if (this.req.body.user_firstname)
        return this.data.user_firstname = String(this.req.body.user_firstname);
    else
        return this.data.user_firstname = false;
};


Validation.prototype.body_user_lastname = function() {
    if (this.req.body.user_lastname)
        return this.data.user_lastname = String(this.req.body.user_lastname);
    else
        return this.data.user_lastname = false;
};
Validation.prototype.body_routepart_description = function() {
    if (this.req.body.routepart_description)
        return this.data.routepart_description = String(this.req.body.routepart_description);
    else
        return this.data.routepart_description = false;
};
Validation.prototype.body_routepart_title = function() {
    if (this.req.body.routepart_title)
        return this.data.routepart_title = String(this.req.body.routepart_title);
    else
        return this.data.routepart_title = false;
};



module.exports = Validation;