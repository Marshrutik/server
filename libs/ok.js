var log = require('./../log');
ok = {};
ok.INSERT_ROUTE  = {code: 'INSERT_ROUTE',    status:201, message:'insert route'};
ok.UPDATE_ROUTE  = {code: 'UPDATE_ROUTE',    status:201, message:'update route'};
ok.DELETE_ROUTE  = {code: 'DELETE_ROUTE',    status:201, message:'delete route'};

ok.INSERT_ROUTEPARTS  = {code: 'INSERT_ROUTEPARTS',     status:201, message:'insert routeparts'};
ok.UPDATE_ROUTEPARTS  = {code: 'UPDATE_ROUTEPARTS',     status:201, message:'update routeparts'};
ok.DELETE_ROUTEPARTS  = {code: 'DELETE_ROUTEPARTS',     status:201, message:'delete routeparts'};

ok.INSERT_USER = {code: 'INSERT_USER', status: 201, message:'insert user'};
ok.UPDATE_USER = {code: 'UPDATE_USER', status: 201, message:'update user'};
ok.DELETE_USER = {code: 'DELETE_USER', status: 201, message:'delete user'};

ok.LOGIN_SUCCESS = {code: 'LOGIN_SUCCESS', status: 200, message: 'login success'};


ok.send = function(res, send){

    if(!send) send = {code: 'OK', status:200, message:'It\'s good'};

    log.info(send.status + ' ' + send.message);
    if(arguments.length==3){
        send.attr = arguments[2];
        log.info("Atrribute:")
        log.info(send.attr);
    }

    res.status(send.status).jsonp(send);

    return true;
};

module.exports = ok;