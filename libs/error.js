var log = require('./../log');
var error = {};

error.BAD_QUERY             = {code : 'BAD_QUERY',          status: 400,    message: 'bad query'};
error.TOKEN_EXPIRED         = {code : 'TOKEN_EXPIRED',      status: 400,    message: 'token expired'};
error.USERNAME_BUSY         = {code : 'USERNAME_BUSY',      status: 400,    message: 'username busy'};
error.EMAIL_BUSY            = {code : 'EMAIL_BUSY',         status: 400,    message: 'email busy'};
error.DUBLICATE_ROUTE       = {code: 'DUBLICATE_ROUTE',     status: 400,    message: 'dublicate route'};
error.DUBLICATE_ROUTEPART   = {code: 'DUBLICATE_ROUTEPART', status: 400,    message: 'dublicate routepart'};

error.INVALID_CREDENTIALS   = {code: 'INVALID_CREDENTIALS', status:401,     message:'invalid credentials'};
error.INVALID_USER          = {code: 'INVALID_USER',        status:401,     message:'invalid user'};
error.INVALID_TOKEN         = {code: 'INVALID_TOKEN',       status:401,     message:'invalid token'};

error.NOT_AUTHORIZED        = {code: 'NOT_AUTHORIZED',      status:403,     message: 'not authorized'};
error.PERMISSION_DENIED     = {code: 'PERMISSION_DENIED',   status:403,     message: 'permission denied'};

error.REGION_NOT_FOUND      = {code: 'REGION_NOT_FOUND',    status:404,     message:'region not found'};
error.COUNTRY_NOT_FOUND     = {code: 'COUNTRY_NOT_FOUND',   status:404,     message:'country not found'};
error.CITY_NOT_FOUND        = {code: 'CITY_NOT_FOUND',      status:404,     message:'city not found'};
error.ROUTE_NOT_FOUND       = {code: 'ROUTE_NOT_FOUND',     status:404,     message:'route not found'};
error.THEME_NOT_FOUND       = {code: 'THEME_NOT_FOUND',     status:404,     message:'theme not found'};
error.TOM_NOT_FOUND         = {code: 'TOM_NOT_FOUND',       status:404,     message:'type of movement not found'};
error.PAGE_NOT_FOUND        = {code: 'PAGE_NOT_FOUND',      status:404,     message:'page not found'};
error.SIGHT_NOT_FOUND       = {code: 'SIGHT_NOT_FOUND',     status:404,     message:'sight not found'};
error.USER_NOT_FOUND        = {code: 'USER_NOT_FOUND',      status:404,     message:'user not found'};
error.ROUTEPART_NOT_FOUND   = {code: 'ROUTEPART_NOT_FOUND', status:404,     message:'routepart not found'};

error.INTERNAL_SERVER_ERROR = {code: 'INTERNAL_SERVER_ERROR',   status:500, message:'internal server error'};
error.NOT_CONNECTION_TO_DB  = {code: 'NOT_CONNECTION_TO_DB',    status:500, message:'not connection to db'};
error.QUERY_TO_DB_INCORRECT  = {code: 'QUERY_TO_DB_INCORRECT',    status:500, message:'query to db incorrect'};



error.send = function(res, err){

    if(!err) err = {code: 'UNKNOWN ERROR', status:500, message:'unknown error'};

        if(err.status == 500) {
            log.error(err.status + ' ' + err.message);
            if(arguments.length==3) {
                err.attr = arguments[2];
                log.error("Error: ");
                log.error(err.attr);
            }
        }
        else {
            log.warn(err.status + ' ' + err.message);
            if(arguments.length==3) {
                err.attr = arguments[2];
                log.warn("Warning: ");
                log.warn(err.attr);
            }
        }
    res.status(err.status).jsonp(err);

    return true;
};

module.exports = error;