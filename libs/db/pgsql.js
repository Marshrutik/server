var _error = require('../error');
var log = require('./../../log');
var pg = require('pg');
var config = require('../config');

module.exports.query = function (query, role, callback) {
    log.info(query);
    pg.connect(connectString(role), function(err, client, done) {
        if(err)
            callback(_error.NOT_CONNECTION_TO_DB);
        else {
            client.query(query, function(err, data){
                done();
                if(err) {
                    log.error(err);
                    callback(_error.QUERY_TO_DB_INCORRECT);
                }
                else
                    callback(null, data);
            })
        }
    })
};

function connectString(role) {
    if (
        role == 'public'    ||
        role == 'user'      ||
        role == 'admin'     ||
        role == 'system'
    ){
        var conString = process.env.DATABASE_URL ||
                "pg://" +   config.get('db:' + role + ':username') +
                ":"     +   config.get('db:' + role + ':password') +
                "@"     +   config.get('db:host') +
                ":"     +   config.get('db:port') +
                "/"     +   config.get('db:name');
        return conString;
    }
    else
        return "";
}