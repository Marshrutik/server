module.exports.dropTriggerAfterApproveRouteIncrementApprovedRoutes = function(){
    return '' +
        'DROP TRIGGER ' +
            'AfterApproveRouteIncrementApprovedRoutes ' +
        'ON ' +
            'Routes'
};
module.exports.addTriggerAfterApproveRouteIncrementApprovedRoutes = function(){
    return '' +
        'CREATE TRIGGER ' +
            'AfterApproveRouteIncrementApprovedRoutes ' +
        'AFTER ' +
            'UPDATE ' +
        'ON ' +
            'Routes ' +
        'FOR EACH ROW ' +
        'EXECUTE PROCEDURE ' +
            'fApproveRouteIncrementApprovedRoutes()'
};
module.exports.functionAfterApproveRouteIncrementApprovedRoutes = function(){
    return '' +
        'CREATE OR REPLACE FUNCTION fApproveRouteIncrementApprovedRoutes() RETURNS TRIGGER AS ' +
        '$BODY$ ' +
        'BEGIN ' +
            'IF (NEW.route_approved = true) THEN ' +
                'UPDATE ' +
                    'Users ' +
                'SET ' +
                    'user_approved_routes = user_approved_routes + 1 ' +
                'WHERE ' +
                    'Users.user_id = NEW.user_id; ' +
            'ELSIF (NEW.route_approved = false) THEN ' +
                'UPDATE ' +
                    'Users ' +
                'SET ' +
                    'user_approved_routes = user_approved_routes + 1 ' +
                'WHERE ' +
                    'Users.user_id = NEW.user_id; ' +
            'END IF; ' +
        'RETURN NEW; ' +
        'END; ' +
        '$BODY$ ' +
        'LANGUAGE plpgsql'
};

module.exports.dropTriggerAfterDeleteRoute = function(){
    return '' +
        'DROP TRIGGER ' +
            'AfterDeleteRoute ' +
        'ON ' +
            'Routes'
};
module.exports.addTriggerAfterDeleteRoute = function(){
    return '' +
        'CREATE TRIGGER ' +
            'AfterDeleteRoute ' +
        'AFTER ' +
            'DELETE ' +
        'ON ' +
            'Routes ' +
        'FOR EACH ROW ' +
        'EXECUTE PROCEDURE ' +
            'fAfterDeleteRoute()'
};
module.exports.functionAfterDeleteRoute = function(){
    return '' +
        'CREATE OR REPLACE FUNCTION fAfterDeleteRoute() RETURNS TRIGGER AS ' +
        '$BODY$ ' +
        'BEGIN ' +
            'IF (OLD.route_approved = true) THEN ' +
                'UPDATE ' +
                    'Users ' +
                'SET ' +
                    'user_approved_routes = user_approved_routes - 1 ' +
                'WHERE ' +
                    'Users.user_id = OLD.user_id; ' +
            'END IF; ' +
        '' +
            'DELETE ' +
            'FROM ' +
                'RouteParts ' +
            'WHERE ' +
                'route_id = OLD.route_id; ' +
        '' +
            'DELETE ' +
            'FROM ' +
                'ThemeRoutes ' +
            'WHERE ' +
                'route_id = OLD.route_id; ' +
        '' +
            'RETURN NEW; ' +
        'END; ' +
        '$BODY$ ' +
        'LANGUAGE plpgsql'
};

module.exports.dropTriggerAfterDeleteUser = function() {
    return '' +
        'DROP TRIGGER ' +
            'AfterDeleteUser ' +
        'ON ' +
            'Users'
};
module.exports.addTriggerAfterDeleteUser = function(){
    return '' +
        'CREATE TRIGGER ' +
            'AfterDeleteUser ' +
        'AFTER ' +
            'DELETE ' +
        'ON ' +
            'Users ' +
        'FOR EACH ROW ' +
        'EXECUTE PROCEDURE ' +
            'fAfterDeleteUser()'
};
module.exports.functionAfterDeleteUser = function(){
    return '' +
        'CREATE OR REPLACE FUNCTION fAfterDeleteUser() RETURNS TRIGGER AS ' +
        '$BODY$ ' +
        'BEGIN ' +
            'DELETE ' +
            'FROM ' +
                'Routes ' +
            'WHERE ' +
                'OLD.user_id = Routes.user_id;' +
        'RETURN NEW; ' +
        'END; ' +
        '$BODY$ ' +
        'LANGUAGE plpgsql'
};
