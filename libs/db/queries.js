module.exports.getInfoAboutCountry = function(data) {
    return '' +
        'SELECT ' +
            '* ' +
        'FROM ' +
            'Countries ' +
        'WHERE ' +
            'country_id = ' + data.country_id;
};

module.exports.getInfoAboutAllCountries = function (){
    return '' +
        'SELECT ' +
            '* ' +
        'FROM ' +
            'Countries'
};

module.exports.getInfoAboutAllRegions = function (data){
    return '' +
        'SELECT ' +
            'region_name,' +
            'region_id ' +
        'FROM ' +
            'Regions ' +
        'WHERE ' +
            'country_id = ' + data.country_id;
};

module.exports.getInfoAboutCity = function(data) {
    return '' +
        'SELECT ' +
            'city_id, ' +
            'city_name ' +
        'FROM ' +
            'Cities ' +
        'WHERE ' +
            'city_id = ' + data.city_id;

};

module.exports.getInfoAboutRegion = function(data) {
    return '' +
        'SELECT ' +
            'region_id, ' +
            'region_name ' +
        'FROM ' +
            'Regions ' +
        'WHERE ' +
            'region_id = ' + data.region_id;

};

module.exports.getInfoAboutCityFromRegion = function(data) {
    return '' +
        'SELECT ' +
            'city_id, ' +
            'city_name ' +
        'FROM ' +
            'Cities ' +
        'WHERE ' +
            'region_id = ' + data.region_id;
};

module.exports.getInfoAboutCityFromCountry = function(data) {
    return '' +
        'SELECT ' +
            'city_id, ' +
            'city_name ' +
        'FROM ' +
            'Cities ' +
        'WHERE ' +
            'country_id = ' + data.country_id;
};

module.exports.getInfoAboutRoutesInCity = function(data) {
    return '' +
        'SELECT ' +
            'route_id, ' +
            'tom.typeofmovement_name,' +
            'route_description,' +
            'route_name,' +
            'route_img_url ' +
        'FROM ' +
            'Routes as r ' +
        'JOIN ' +
            'Typesofmovement as tom ' +
        'ON ' +
            '(r.typeofmovement_id = tom.typeofmovement_id) ' +
        'WHERE ' +
            'city_id = ' + data.city_id + ' AND ' +
            'route_approved = ' + true + ' ' +
        'ORDER BY ' +
            'route_id'
};

module.exports.getInfoAboutThemesInRoute = function(data) {
    var query = 'SELECT ' +
                    'th.theme_name, route_id ' +
                'FROM ' +
                    'Themes as th ' +
                'JOIN ' +
                    'Themeroutes as tr ' +
                'ON ' +
                    '(th.theme_id = tr.theme_id) ' +
                'WHERE ';
    for(var i = 0; i < data.length; i++) {
        query += 'route_id = ' + data[i].route_id;
        if(i<data.length-1)
            query += ' OR ';
    }
    return query;

};

module.exports.getInfoAboutRoutePart = function(data) {
    return '' +
        'SELECT ' +
            '* ' +
        'FROM ' +
            'RouteParts ' +
        'WHERE ' +
            'rp_id = ' + data.routepart_id;
};

module.exports.getInfoAboutRoute = function (data) {
    return '' +
        'SELECT ' +
            'route_id, ' +
            'tom.typeofmovement_name,' +
            'route_description,' +
            'route_name,' +
            'route_img_url ' +
        'FROM ' +
            'Routes as r ' +
        'JOIN ' +
            'Typesofmovement as tom ' +
        'ON ' +
            '(r.typeofmovement_id = tom.typeofmovement_id) ' +
        'WHERE ' +
            'route_id = ' + data.route_id
};

module.exports.insertInfoAboutRoute = function (data) {
  return '' +
      'INSERT INTO ' +
        'Routes ' +
      '( ' +
        'route_name, ' +
        'user_id, ' +
        'city_id, ' +
        'route_description, ' +
        'typeofmovement_id, ' +
        'route_approved' +
      ') ' +
      'VALUES ' +
      '( ' +
          '\'' +    data.route_name         + '\''  + ', '  +
                    data.user_id                    + ', '  +
                    data.city_id                    + ', '  +
          '\'' +    data.route_description  + '\''  + ', '  +
                    data.typeofmovement_id          + ', '  +
                    false                                   +
      ') ' +
      'RETURNING ' +
        'route_id'
};

module.exports.deleteInfoAboutRoute = function(data) {
    return '' +
        'DELETE ' +
        'FROM ' +
            'Routes ' +
        'WHERE ' +
            'route_id = ' + data.route_id;
};

module.exports.updateInfoAboutRoute = function(data) {
    return '' +
        'UPDATE ' +
            'Routes ' +
        'SET ' +
            'route_name = '         +  '\''  +  data.route_name         + '\', ' +
            'city_id = '            +           data.city_id            + ', '   +
            'route_description = '  +   '\'' +  data.route_description  + '\', ' +
            'typeofmovement_id = '  +           data.typeofmovement_id  + ' '    +
        'WHERE ' +
            'route_id = ' + data.route_id;
};

module.exports.getInfoAboutRouteFromUser = function(data) {
    return '' +
        'SELECT ' +
            'route_id, ' +
            'user_id ' +
        'FROM ' +
            'Routes ' +
        'WHERE ' +
            'route_id = '   + data.route_id + ' AND ' +
            'user_id = '    + data.user_id;
};

module.exports.getInfoAboutSight = function(data){
    return '' +
        'SELECT ' +
            'sight_name,' +
            'city_id,' +
            'sight_latitude,' +
            'sight_longitude ' +
        'FROM ' +
            'Sights ' +
        'WHERE ' +
            'sight_id = ' + data.sight_id;
};

module.exports.getInfoAboutTheme = function(data){
    return '' +
        'SELECT ' +
            '* ' +
        'FROM ' +
            'Themes ' +
        'WHERE ' +
            'theme_id = ' + data.theme_id;
};

module.exports.getInfoAboutRouteInThemes = function(data){
    return '' +
        'SELECT ' +
            '* ' +
        'FROM ' +
            'ThemeRoutes ' +
        'WHERE ' +
            'theme_id = ' + data.theme_id;
};

module.exports.getInfoAboutTypesOfMovement = function(data){
    return '' +
        'SELECT ' +
            '* ' +
        'FROM ' +
            'TypesOfMovement ' +
        'WHERE ' +
            'typeofmovement_id = ' + data.typeofmovement_id;
};

module.exports.getAccuracyInfoAboutRoute = function(data) {
    return '' +
        'SELECT ' +
            'route_name, ' +
            'city_id, ' +
            'typeofmovement_id ' +
        'FROM ' +
            'Routes ' +
        'WHERE ' +
            'route_name = ' + '\'' + data.route_name + '\'' + ' AND ' +
            'city_id = ' + data.city_id + ' AND ' +
            'typeofmovement_id = ' + data.typeofmovement_id;
};

module.exports.getAccuracyInfoAboutRoutePart = function(data) {
    return '' +
        'SELECT ' +
            'rp_id, ' +
            'rp_start_latitude,' +
            'rp_start_longitude,' +
            'rp_end_latitude,' +
            'rp_end_longitude,' +
            'user_id,' +
            'route_id,' +
            'part_id ' +
        'FROM ' +
            'Routeparts ' +
        'WHERE ' +
        '(' +
            'rp_start_latitude  = ' + '\'' + data.routepart_start_latitude + '\''     + ' AND ' +
            'rp_start_longitude = ' + '\'' + data.routepart_start_longitude + '\''    + ' AND ' +
            'rp_end_latitude    = ' + '\'' + data.routepart_end_latitude + '\''       + ' AND ' +
            'rp_end_longitude   = ' + '\'' + data.routepart_end_longitude + '\''      + ' AND ' +
            'user_id            = ' + data.user_id                      + ' AND ' +
            'route_id           = ' + data.route_id                     + ' AND ' +
            'part_id            = ' + data.part_id + '' +
        ')' +
            ' OR ' +
        '(' +
            'user_id            = ' + data.user_id                      + ' AND ' +
            'route_id           = ' + data.route_id                     + ' AND ' +
            'part_id            = ' + data.part_id + '' +
        ')';
};

module.exports.insertInfoAboutRoutePart = function (data) {
    return '' +
        'INSERT ' +
        'INTO ' +
            'Routeparts ' +
        '( ' +
            'rp_start_latitude,' +
            'rp_start_longitude,' +
            'rp_end_latitude,' +
            'rp_end_longitude,' +
            'user_id,' +
            'route_id,' +
            'part_id,' +
            'rp_description,' +
            'rp_title' +
        ') ' +
        'VALUES ' +
        '( ' +
        '\'' + data.routepart_start_latitude     +'\'' +  ', ' +
        '\'' + data.routepart_start_longitude    +'\'' +  ', ' +
        '\'' + data.routepart_end_latitude       +'\'' +  ', ' +
        '\'' + data.routepart_end_longitude      +'\'' +  ', ' +
        '\'' + data.user_id                      +'\'' +  ', ' +
        '\'' + data.route_id                     +'\'' +  ', ' +
        '\'' + data.part_id                      +'\'' +  ', ' +
        '\'' + data.routepart_description    + '\''   + ',  ' +
        '\'' + data.routepart_title    + '\''   + '  ' +
        ') ' +
        'RETURNING ' +
            'rp_id'
};

module.exports.getInfoAboutRoutePartFromUser = function (data) {
    return '' +
        'SELECT ' +
            'rp_id, ' +
            'user_id ' +
        'FROM ' +
            'RouteParts ' +
        'WHERE ' +
            'rp_id      = '     + data.routepart_id + ' AND ' +
            'user_id    = '     + data.user_id;
};

module.exports.deleteInfoAboutRoutePart = function(data) {
    return '' +
        'DELETE ' +
        'FROM ' +
            'RouteParts ' +
        'WHERE ' +
            'routepart_id = ' + data.routepart_id;
};

module.exports.updateInfoAboutRoutePart = function(data) {
    return '' +
        'UPDATE ' +
            'Routeparts ' +
        'SET ' +
            'rp_start_latitude = '      + '\'' + data.routepart_start_latitude     + '\'' + ', ' +
            'rp_start_longitude = '     + '\'' + data.routepart_start_longitude    + '\'' + ', ' +
            'rp_end_latitude = '        + '\'' + data.routepart_end_latitude       + '\'' + ', ' +
            'rp_end_longitude = '       + '\'' + data.routepart_end_longitude      + '\'' + ', ' +
            'rp_description = '         + '\'' + data.routepart_description        + '\'' + ', ' +
            'rp_title  = '              + '\'' + data.routepart_title              + '\'' + ', ' +
            'part_id = '                +        data.part_id                             + ' '  +
        'WHERE ' +
            'rp_id = ' + data.routepart_id;
};

module.exports.insertNewUser = function(data) {
    return '' +
        'INSERT ' +
        'INTO '                  +
            'Users '                    +
        '('                             +
            'user_username, '           +
            'user_password, '           +
            'user_rating, '             +
            'user_approved_routes, '    +
            'user_email, '              +
            'user_firstname, '          +
            'user_lastname, '           +
            'city_id, '                 +
            'user_role '                +
        ') '                            +
        'VALUES '                       +
        '( '                             +
            '\'' +  data.user_username   + '\'' + ', '   +
            '\'' +  data.user_password   + '\'' + ', '   +
                    0                      + ', '   +
                    0                      + ', '   +
            '\'' +  data.user_email      + '\'' + ', '   +
            '\'' +  data.user_firstname  + '\'' + ', '   +
            '\'' +  data.user_lastname   + '\'' + ', '   +
                    data.city_id           + ', '   +
            '\'' + 'user'           + '\'' + '  '   +
        ') ' +
        'RETURNING ' +
            'user_id'
};

module.exports.getInfoAboutUsername = function(data) {
    return '' +
        'SELECT ' +
            'user_username,' +
            'user_firstname,' +
            'user_lastname ' +
        'FROM ' +
            'Users ' +
        'WHERE ' +
            'user_username = ' + '\'' + data.user_username + '\'' + ' AND ' +
            'user_password = ' + '\'' + data.user_password + '\''
};

module.exports.getUsername = function(data) {
    return '' +
        'SELECT ' +
            'user_username ' +
        'FROM ' +
            'Users ' +
        'WHERE ' +
            'user_username = ' + '\'' + data.user_username + '\'';
};

module.exports.getEmail = function(data) {
    return '' +
        'SELECT ' +
            'user_email ' +
        'FROM ' +
            'Users ' +
        'WHERE ' +
            'user_email = ' + '\'' + data.user_email + '\'';
};

module.exports.getInfoAboutUser = function(data) {
    return '' +
        'SELECT ' +
            'user_id, ' +
            'user_username, ' +
            'user_firstname, ' +
            'user_lastname, ' +
            'city_id, ' +
            'user_role ' +
        'FROM ' +
            'Users ' +
        'WHERE ' +
            'user_token = '     + '\'' + data.user_token      + '\''

};

module.exports.getInfoAboutRoutePartsInRoute = function(data) {
    return '' +
        'SELECT ' +
            'rp_id,' +
            'part_id,' +
            'rp_start_latitude,' +
            'rp_start_longitude, ' +
            'rp_end_latitude,' +
            'rp_end_longitude,' +
            'rp_description,' +
            'rp_title ' +
        'FROM ' +
            'RouteParts ' +
        'WHERE ' +
            'route_id = ' + data.route_id;
};

module.exports.getInfoAboutRouteOfUser = function (data) {
    return '' +
        'SELECT ' +
            'route_id, ' +
            'tom.typeofmovement_name,' +
            'route_description,' +
            'route_name,' +
            'route_img_url ' +
        'FROM ' +
            'Routes as r ' +
        'JOIN ' +
            'Typesofmovement as tom ' +
        'ON ' +
            '(r.typeofmovement_id = tom.typeofmovement_id) ' +
        'JOIN ' +
            'Users as u ' +
        'ON ' +
            '(u.user_id = r.user_id) AND ' +
            '(u.user_username = ' + '\'' + data.user_username + '\'' + ')'
};

module.exports.getInfoAboutSightInCity = function(data) {
    return '' +
        'SELECT ' +
            'sight_name, ' +
            'sight_longitude,' +
            'sight_latitude ' +
        'FROM ' +
            'Sights ' +
        'WHERE ' +
            'city_id = ' + data.city_id;
}

module.exports.updateToken = function(data) {
    return '' +
        'UPDATE ' +
            'Users ' +
        'SET ' +
            'user_token = ' + '\'' + data.user_token + '\' ' +
        'WHERE ' +
            'user_username = ' + '\'' + data.user_username + '\''
}




