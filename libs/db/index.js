var _error = require('../error');
var pgsql = require('./pgsql');
var queries = require('./queries');
var Validation = require('../validation');
var async = require('async');
var triggers = require('./triggers');
var indexes = require('./indexes');

var user_role_system = 'system'

module.exports.getInfoAboutCountry = function(req, callback) {

    validate = new Validation(req);
    validate.params_country_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutCountry(validate.getData()), req.user.user_role, function (err, info) {
            if (err)
                callback(err);
            else if (!info.rowCount)
                callback(_error.COUNTRY_NOT_FOUND);
            else
                callback(null, info.rows[0]);
        })
    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.getInfoAboutAllCountries = function(req, callback) {
    pgsql.query(queries.getInfoAboutAllCountries(), req.user.user_role, function(err, info) {
        if(err)
            callback(err);
        else if (!info.rowCount)
            callback(_error.COUNTRY_NOT_FOUND);
        else
            callback(null, info.rows);
    })
};

module.exports.getInfoAboutCity = function(req, callback) {

    validate = new Validation(req);
    validate.params_city_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutCity(validate.getData()),req.user.user_role, function (err, info) {
            if (err)
                callback(err);
            else if (!info.rowCount)
                callback(_error.CITY_NOT_FOUND);
            else
                callback(null, info.rows[0]);
        })
    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.getInfoAboutCityFromRegion = function(req, callback) {

    validate = new Validation(req);
    validate.query_region_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutCityFromRegion(validate.getData()), req.user.user_role, function (err, info) {
            if (err)
                callback(err);
            else if (!info.rowCount)
                callback(_error.CITY_NOT_FOUND);
            else
                callback(null, info.rows);
        })
    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.getInfoAboutCityFromCountry = function(req, callback) {

    validate = new Validation(req);
    validate.query_country_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutCityFromCountry(validate.getData()), req.user.user_role, function (err, info) {
            if (err)
                callback(err);
            else if (!info.rowCount)
                callback(_error.CITY_NOT_FOUND);
            else
                callback(null, info.rows);
        })
    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.getInfoAboutRoutesInCity = function(req, callback) {

    validate = new Validation(req);
    validate.params_city_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutRoutesInCity(validate.getData()), req.user.user_role, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.ROUTE_NOT_FOUND);
            else {
                _getInfoAboutThemesInRoute(info.rows, function(err, data){
                    if(err)
                        callback(err);
                    else
                        callback(null, data);
                })
            }
        })
    }
};

module.exports.getInfoAboutRegion = function(req, callback) {

    validate = new Validation(req);
    validate.params_region_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutRegion(validate.getData()), req.user.user_role, function (err, info) {
            if (err)
                callback(err);
            else if (!info.rowCount)
                callback(_error.REGION_NOT_FOUND);
            else
                callback(null, info.rows[0]);
        })
    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.getInfoAboutRoutePart = function(req, callback) {

    validate = new Validation(req);
    validate.params_routepart_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutRoutePart(validate.getData()), req.user.user_role, function (err, info) {
            if (err)
                callback(err);
            else if (!info.rowCount)
                callback(_error.ROUTEPART_NOT_FOUND);
            else
                callback(null, info.rows[0]);
        });
    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.getInfoAboutRoute = function(req, callback) {

    validate = new Validation(req);
    validate.params_route_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutRoute(validate.getData()), req.user.user_role, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.ROUTE_NOT_FOUND);
            else {
                _getInfoAboutThemesInRoute(info.rows, function(err, data){
                    if(err)
                        callback(err);
                    else
                        callback(null, data);
                })
            }
        })
    }
    else
        callback (_error.BAD_QUERY);
};

module.exports.insertIntoAboutRoute = function(req, callback){

    validate = new Validation(req);
    validate.body_route_name();
    validate.user_user_id();
    validate.body_city_id();
    validate.body_typeofmovement_id();
    validate.body_route_description();

    if(validate.checkData()) {
        _checkDublicateRoute(validate.getData(), function(err, info) {
            if(err)
                callback(err);
            else if(info)
                callback(_error.DUBLICATE_ROUTE);
            else {
                pgsql.query(queries.insertInfoAboutRoute(validate.getData()), req.user.user_role, function (err, info) {
                    if (err)
                        callback(err);
                    else
                        callback(null, info.rows[0]);
                })
            }
        })

    }
    else
        callback(_error.BAD_QUERY);
};

module.exports.deleteInfoAboutRoute = function(req, callback){

    validate = new Validation(req);
    validate.params_route_id();

    if(validate.checkData()) {
        _accessRoute(validate.getData(), function (err, info) {
            if (err)
                callback(err);
            else if (!info) {
                callback(_error.PERMISSION_DENIED);
            }
            else {
                pgsql.query(queries.deleteInfoAboutRoute(validate.getData()), req.user.user_role, function (err, info) {
                    if (err)
                        callback(err);
                    else
                        callback(null, info);
                })
            }
        })
    }
    else
        callback(_error.BAD_QUERY);

};

module.exports.updateInfoAboutRoute = function(req, callback){

    validate = new Validation(req);
    validate.params_route_id()
    validate.user_user_role();
    validate.user_user_id();
    validate.body_route_name();
    validate.body_city_id();
    validate.body_route_description();
    validate.body_typeofmovement_id();

    if(validate.checkData()){
        _accessRoute(validate.getData(), function(err, info)
        {
            if(err)
                callback(err);
            else if(!info){
                callback(_error.PERMISSION_DENIED);
            }
            else {
                pgsql.query(queries.updateInfoAboutRoute(validate.getData()), req.user.user_role, function (err, data) {
                    if (err)
                        callback(err);
                    else if (!data.rowCount)
                        callback(_error.ROUTE_NOT_FOUND);
                    else
                        callback(null, data);
                })
            }
        })
    }
    else
        callback(_error.BAD_QUERY)
};

module.exports.getInfoAboutSight = function(req, callback){

    validate = new Validation(req);
    validate.params_sight_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutSight(validate.getData()), req.user.user_role, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.SIGHT_NOT_FOUND);
            else
                callback(null, info.rows[0]);
        })
    }
    else
        callback (_error.BAD_QUERY);
};

module.exports.getInfoAboutTheme = function(req, callback){
    validate = new Validation(req);
    validate.params_theme_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutTheme(validate.getData()), req.user.user_role, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.THEME_NOT_FOUND);
            else
                callback(info.rows[0]);
        })
    }
    else
        callback (_error.BAD_QUERY);
};

module.exports.getInfoAboutRouteInThemes = function(req, callback){
    validate = new Validation(req);
    validate.params_theme_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutRouteInThemes(validate.getData()), req.user.user_role, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.THEME_NOT_FOUND);
            else
                callback(info.rows);
        })
    }
    else
        callback (_error.BAD_QUERY);
};

module.exports.getInfoAboutTypesOfMovement = function(req, callback){
    validate = new Validation(req);
    validate.params_typeofmovement_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutTypesOfMovement(validate.getData()), req.user.user_role, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.TOM_NOT_FOUND);
            else
                callback(info.rows[0]);
        })
    }
    else
        callback (_error.BAD_QUERY);
};

module.exports.insertInfoAboutRoutePart = function(req, callback){

    validate = new Validation(req);
    validate.body_routepart_start_latitude();
    validate.body_routepart_start_longitude();
    validate.body_routepart_end_latitude();
    validate.body_routepart_end_longitude();
    validate.body_route_id();
    validate.body_part_id();
    validate.user_user_id();
    validate.body_routepart_description();
    validate.body_routepart_title();
    var nonStaticVariable = validate.getData(); //Какого то хрена у меня работает статическое своейство класса

    if(validate.checkData()) {
        _checkDublicateRoutePart(nonStaticVariable, function(err, info) {
            if(err)
                callback(err);
            else if(info) {
                callback(null, {"routepart_id" : info.rp_id, "error" : _error.DUBLICATE_ROUTEPART});
            }
            else {
                pgsql.query(queries.insertInfoAboutRoutePart(nonStaticVariable), req.user.user_role, function (err, data) {
                    if (err)
                        callback(err);
                    else
                        callback(null, {"routepart_id" : data.rows[0].rp_id});
                })
            }
        })

    }
    else
        callback(_error.BAD_QUERY);
}

module.exports.insertInfoAboutAllRouteParts = function(req, callback){
    try {
        arrayRouteParts = req.body.array_parts || '';
        if(arrayRouteParts == '')
            callback(_error.BAD_QUERY);
        for (var i = 0; i < arrayRouteParts.length; i++) {
            arrayRouteParts[i].body = arrayRouteParts[i];
            arrayRouteParts[i].user = req.user;
        }
    }
    catch (e) {
        callback(e);
    }

    async.map(
        arrayRouteParts,
        module.exports.insertInfoAboutRoutePart,
        function(err, data){
            if(err) {
                callback(err);
            }
            else
                callback(null, data);
        })
};

module.exports.updateInfoAllRouteParts = function(req, callback){
    try {
        arrayRouteParts = req.body.array_parts || '';
        if(arrayRouteParts == '')
            callback(_error.BAD_QUERY);
        for (var i = 0; i < arrayRouteParts.length; i++) {
            arrayRouteParts[i].body = arrayRouteParts[i];
            arrayRouteParts[i].user = req.user;
        }
    }
    catch (e) {
        callback(e);
    }

    async.map(
        arrayRouteParts,
        module.exports.updateInfoAboutRoutePart,
        function(err, data){
            if(err) {
                callback(err);
            }
            else
                callback(null, data);
        })
};

module.exports.updateInfoAboutRoutePart = function(req, callback) {

    validate = new Validation(req);
    validate.body_routepart_start_latitude();
    validate.body_routepart_start_longitude();
    validate.body_routepart_end_latitude();
    validate.body_routepart_end_longitude();
    validate.user_user_id();
    validate.user_user_role();
    validate.body_part_id();
    validate.body_routepart_id();
    validate.body_routepart_description();
    validate.body_routepart_title();

    var nonStaticDataValue = validate.getData();
    var nonStaticCheckValue = validate.checkData();
    if(nonStaticCheckValue){
        _accessRoutePart(nonStaticDataValue, function(err, info) {
            if(err)
                callback(err);
            else if(!info){
                callback(_error.PERMISSION_DENIED);
            }
            else {
                pgsql.query(queries.updateInfoAboutRoutePart(nonStaticDataValue), req.user.user_role, function (err, data) {
                    if (err)
                        callback(err);
                    else if (!data.rowCount)
                        callback(_error.ROUTEPART_NOT_FOUND);
                    else
                        callback(null, data);
                })
            }
        })
    }
    else
        callback(_error.BAD_QUERY)
};

module.exports.deleteInfoAboutRoutePart = function(req, callback){

    validate = new Validation(req);
    validate.params_routepart_id();

    if(validate.checkData()) {
        _accessRoutePart(validate.getData(), function (err, info) {
            if (err)
                callback(err);
            else if (!info) {
                callback(_error.PERMISSION_DENIED);
            }
            else {
                pgsql.query(queries.deleteInfoAboutRoutePart(validate.getData()), req.user.user_role, function (err, info) {
                    if (err)
                        callback(err);
                    else
                        callback(null, info);
                })
            }
        })
    }
    else
        callback(_error.BAD_QUERY);

};

module.exports.insertNewUser = function(req, callback) {


    validate = new Validation(req);
    validate.body_user_username();
    validate.body_user_password();
    validate.body_user_email();
    validate.body_user_firstname();
    validate.body_user_lastname();
    validate.body_city_id();

    if(!validate.checkData()){
        return callback(_error.BAD_QUERY);
    }
    _checkDublicateUsername(validate.getData(), function(err, data){
        if(err)
            callback(err);
        else {
            _checkDublicateEmail(validate.getData(), function(err, data){
                if(err)
                    callback(err);
                else {
                    pgsql.query(queries.insertNewUser(validate.getData()), user_role_system, function(err, data){
                        if(err)
                            callback(err);
                        else
                            callback(null, data.rows[0]);
                    })
                }
            })
        }

    })
};

module.exports.getInfoAboutUsername = function(req, callback) {
    validate = new Validation(req);
    validate.body_user_username();
    validate.body_user_password();

    pgsql.query(queries.getInfoAboutUsername(validate.getData()), user_role_system, function(err, info){
        if (err)
            callback(_error.BAD_QUERY);
        else if (!info.rowCount)
            callback(_error.INVALID_CREDENTIALS);
        else

            callback(null, info.rows[0]);
    })
};

module.exports.getInfoAboutUser = function(data, callback) {

    validate = new Validation(data);
    validate.body_user_token();

    pgsql.query(queries.getInfoAboutUser(validate.getData()), user_role_system, function(err, info){
        if (err)
            callback(err);
        else
            callback(null, info.rows[0]);
    })
};

module.exports.getAllInfoAboutRoute = function(data, callback) {
    validate = new Validation(data);
    validate.params_route_id();

    if(validate.checkData()) {
        pgsql.query(queries.getInfoAboutRoute(validate.getData()), user_role_system, function(err, info){
            if(err)
                callback(err);
            else if(!info.rowCount)
                callback(_error.ROUTE_NOT_FOUND);
            else {
                _getInfoAboutRoutePartsInRoute(info.rows, function(err, fact){
                    if(err)
                        callback(err);
                    else
                        callback(null, fact);
                })
            }
        })
    }
    else
        callback (_error.BAD_QUERY);
}

module.exports.updateTriggerAfterApproveRouteIncrementApprovedRoutes = function() {
    dropTrigger();
    function dropTrigger() {
        pgsql.query(triggers.dropTriggerAfterApproveRouteIncrementApprovedRoutes(), user_role_system, createFunction);
    }
    function createFunction (err, data) {
        pgsql.query(triggers.functionAfterApproveRouteIncrementApprovedRoutes(), user_role_system, createTrigger)
    }
    function createTrigger(err, data) {
        pgsql.query(triggers.addTriggerAfterApproveRouteIncrementApprovedRoutes(), user_role_system, end)
    }
    function end(err, data) {}

};
module.exports.updateTriggerAfterDeleteRoute = function() {
    dropTrigger();
    function dropTrigger() {
        pgsql.query(triggers.dropTriggerAfterDeleteRoute(), user_role_system, createFunction);
    }
    function createFunction (err, data) {
        pgsql.query(triggers.functionAfterDeleteRoute(), user_role_system, createTrigger)
    }
    function createTrigger(err, data) {
        pgsql.query(triggers.addTriggerAfterDeleteRoute(), user_role_system, end)
    }
    function end(err, data) {}
};
module.exports.updateTriggerAfterDeleteUser = function() {
    dropTrigger();
    function dropTrigger() {
        pgsql.query(triggers.dropTriggerAfterDeleteUser(), user_role_system, createFunction);
    }
    function createFunction (err, data) {
        pgsql.query(triggers.functionAfterDeleteUser(), user_role_system, createTrigger)
    }
    function createTrigger(err, data) {
        pgsql.query(triggers.addTriggerAfterDeleteUser(), user_role_system, end)
    }
    function end(err, data) {}
};

function _getInfoAboutThemesInRoute(data, callback){
    pgsql.query(queries.getInfoAboutThemesInRoute(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else {
            for(var i = 0; i < data.length; i++){
                data[i].themes = [];
                for(var j = 0; j < info.rowCount; j++){
                    if(info.rows[j].route_id == data[i].route_id) {
                        data[i].themes.push(info.rows[j].theme_name);
                    }
                }
            }
            callback(null, data);
        }
    })
};
function _getInfoAboutRoutePartsInRoute(data, callback) {
    _getInfoAboutThemesInRoute(data, function(err, info) {
        if(err)
            return callback(err);
        pgsql.query(queries.getInfoAboutRoutePartsInRoute(info[0]), user_role_system, function (err, fact) {
            if (err)
                callback(err);
            else {
                info[0].routeparts = fact.rows;
                callback(null, info);
            }
        })
    })
};

module.exports.getInfoAboutRouteOfUser = function(req, callback) {

    pgsql.query(queries.getInfoAboutRouteOfUser(req.user), user_role_system, function(err, data){
        if(err)
            callback(err);
        else if(!data.rowCount)
            callback(_error.ROUTE_NOT_FOUND);
        else {
            _getInfoAboutRouteOfUserArray(data.rows, function(err, info){
                if(err)
                    callback(err);
                else
                    callback(null, info);
            })

        }
            })

};
function _getInfoAboutRouteOfUserArray(data, callback){
    _getInfoAboutThemesInRoute(data, function(err, info){
        if(err)
            callback (err)
        else {
            function _getInfoAboutRouteParts(info, callback){
                pgsql.query(queries.getInfoAboutRoutePartsInRoute(info), user_role_system, function (err, fact) {
                    if (err)
                        callback(err);
                    else {
                        info.routeparts = fact.rows;
                        callback(null, info);
                    }
                })
            }
            async.map(
                info,
                _getInfoAboutRouteParts,
                function(err, fact){
                    if (err)
                        callback(_error.INTERNAL_SERVER_ERROR);
                    else
                        callback(null, fact);
                }
            )

        }
    })
};
module.exports.updateToken = function (data, callback) {
    pgsql.query(queries.updateToken(data), user_role_system, function (err, info) {
        if (err)
            callback(err);
        else {
            callback(null);
        }
    })
}

module.exports.getInfoAboutSightInCity = function(req, callback) {
    validate = new Validation(req);
    validate.params_city_id();

    if (!validate.checkData())
        return callback(_error.BAD_QUERY);
    pgsql.query(queries.getInfoAboutSightInCity(validate.getData()), req.user.user_role, function(err, info){
        if (err)
            callback(err);
        else
            callback(null, info.rows);
    })
}

module.exports.updateIndexUsers = function() {
    pgsql.query(indexes.dropIndexUsers(), user_role_system, function(err, info){
        pgsql.query(indexes.addIndexUsers(), user_role_system, function(err, info){
        })
    })
};
module.exports.updateIndexRouteParts = function() {
    pgsql.query(indexes.dropIndexRouteParts(), user_role_system, function(err, info){
        pgsql.query(indexes.addIndexRouteParts(), user_role_system, function(err, info){
        })
    })
};
function _checkDublicateRoute(data, callback){
    pgsql.query(queries.getAccuracyInfoAboutRoute(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else if (info.rowCount)
            callback(null, true);
        else
            callback(null, false);
    })
};
function _checkDublicateRoutePart(data, callback){
    pgsql.query(queries.getAccuracyInfoAboutRoutePart(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else if (info.rowCount)
            callback(null, info.rows[0]);
        else
            callback(null, false);
    })
};
function _checkDublicateUsername(data, callback){
    pgsql.query(queries.getUsername(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else if(info.rowCount)
            callback(_error.USERNAME_BUSY);
        else
            callback(null, false);
    })
}
function _checkDublicateEmail(data, callback){
    pgsql.query(queries.getEmail(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else if(info.rowCount)
            callback(_error.EMAIL_BUSY);
        else
            callback(null, false);
    })
}
function _accessRoute(data, callback) {
    pgsql.query(queries.getInfoAboutRouteFromUser(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else if (info.rowCount && data.user_role == 'user')
            callback(null, true);
        else if (data.user_role == 'admin')
            callback(null, true);
        else
            callback(null, false);
    })
};
function _accessRoutePart(data, callback) {
    pgsql.query(queries.getInfoAboutRoutePartFromUser(data), user_role_system, function(err, info){
        if(err)
            callback(err);
        else if (info.rowCount && data.user_role == 'user')
            callback(null, true);
        else if (data.user_role == 'admin')
            callback(null, true);
        else
            callback(null, false);
    })
};


