module.exports.dropIndexRouteParts = function(){
    return '' +
        'DROP INDEX ' +
            'iRouteParts ' +
        'ON ' +
            'RouteParts'
};
module.exports.addIndexRouteParts = function(){
    return '' +
        'CREATE INDEX ' +
            'iRouteParts ' +
        'ON ' +
            'RouteParts ' +
        '(' +
            'route_id,' +
            'part_id' +
        ')'
};
module.exports.addIndexUsers = function(){
    return '' +
        'CREATE INDEX ' +
            'iUsers ' +
        'ON ' +
            'Users ' +
        '(' +
            'user_username,' +
            'user_email' +
        ')'
};
module.exports.dropIndexUsers = function(){
    return '' +
        'DROP INDEX ' +
            'iUsers ' +
        'ON ' +
            'Users '
};