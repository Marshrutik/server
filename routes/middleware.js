var _error = require('../libs/error');
var log = require('./../log')
var db = require('../libs/db');
var security = require('../libs/security');

module.exports.verify = function(req, res, next) {
    var token = (req.body && req.body.token);
    if (!token)
        return _error.send(res, _error.INVALID_TOKEN);
    try {
        try {
            var decoded = security.decodeToken(token);
        }
        catch (e) {
            return _error.send(res, _error.INVALID_TOKEN);
        }
        decoded.body = decoded;
        decoded.body.user_token = token;
        if (decoded.exp <= Date.now())
            return _error.send(res, _error.TOKEN_EXPIRED);
    }
    catch (err) {
        return _error.send(res, _error.INTERNAL_SERVER_ERROR, err);
    }
    db.getInfoAboutUser(decoded, function (err, infoUser) {
        if (err)
            return _error.send(res, err);
        else {
            if (infoUser){
                req.user = infoUser;
                req.user.user_token = token;
                next();
            }
            else
                return _error.send(res, _error.INVALID_USER);
        }
    })
}