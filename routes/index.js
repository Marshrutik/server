var log = require('./../log');
var express = require('express');

module.exports = function (app) {

    countries = require('../handlers/countries');
    regions = require('../handlers/regions');
    cities = require('../handlers/cities');
    routes = require('../handlers/routes');
    themes = require('../handlers/themes');
    typesofmovement = require('../handlers/typesofmovement');
    routeparts = require('../handlers/routeparts');
    sights = require('../handlers/sights');
    auth = require('../handlers/auth');

    middleware = require('./middleware');
    permission = require('./permission');

    app.use(express.bodyParser());
    app.use(express.methodOverride());

    app.all('/*', function(req, res, next){
        req.user = {user_role : 'public'};
        next();
    })
    app.get('/', function(req, res){
        res.send(req.user);
    });


    app.get(    '/api/countries',                           permission.access('public'), countries.list);
    app.get(    '/api/regions',                             permission.access('public'), regions.list);
    app.get(    '/api/cities',                              permission.access('public'), cities.list);

    app.get(    '/api/countries/:country_id',               permission.access('public'), countries.get);
    app.get(    '/api/regions/:region_id',                  permission.access('public'), regions.get);
    app.get(    '/api/cities/:city_id',                     permission.access('public'), cities.get);
    app.get(    '/api/cities/:city_id/routes',              permission.access('public'), cities.getRoutes);
    app.get(    '/api/cities/:city_id/sights',              permission.access('public'), cities.getSights);

    app.get(    '/api/routes/:route_id',                    permission.access('public'), routes.getAll);
    app.post(   '/api/routes/',                             middleware.verify, permission.access('user'), routes.post);
    app.delete( '/api/routes/:route_id',                    middleware.verify, permission.access('user'), routes.delete);
    app.put(    '/api/routes/:route_id',                    middleware.verify, permission.access('user'), routes.put);

    app.post(   '/api/routes/user',                         middleware.verify, permission.access('user'), routes.getInfoAboutRouteOfUser);

    app.get(    '/api/themes/:theme_id',                    permission.access('public'), themes.get);
    app.get(    '/api/themes/:theme_id/routes',             permission.access('public'), themes.list);

    app.get(    '/api/typesofmovement/:typeofmovement_id',  permission.access('public'), typesofmovement.get);

    app.get(    '/api/routeparts/:routepart_id',            permission.access('public'), routeparts.get);
    app.post(   '/api/routeparts/',                         middleware.verify, permission.access('user'), routeparts.post);
    app.post(   '/api/routeparts/one/',                     middleware.verify, permission.access('user'), routeparts.postOne);
    app.delete( '/api/routeparts/:routepart_id',            middleware.verify, permission.access('user'), routes.delete);
    app.put(    '/api/routeparts/:routepart_id',            middleware.verify, permission.access('user'), routeparts.put);
    app.put(    '/api/routeparts/',                         middleware.verify, permission.access('user'), routeparts.updateInfoAllRouteParts);

    app.get(    '/api/sights/:sight_id',                    permission.access('public'), sights.get);



    app.post(   '/api/login',                               permission.access('public'), auth.login);
    app.post(   '/api/signup',                              permission.access('public'), auth.signup);


    log.info('Routes connect');
};