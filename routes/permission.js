var _error   = require('../libs/error');

var levels = {
    public  : ['public', 'user', 'admin'],
    user    : ['user', 'admin'],
    admin   : ['admin']
};

function check(level, role) {
    var roles = levels[level];

    if (!roles) return false;

    return roles.indexOf(role) !== -1;
}


module.exports.access = function (level) {
    return function (req, res, next) {
        if(req.body) {
            console.log(    " --------------------- Begin Request --------------------- \n",
                        req.body,
                            "\n --------------------- End Request ----------------------- ");
        }
        if (req.user && check(level, req.user.user_role)) {
            return next();
        }
        console.log(req.user, level, req.user.user_role);
        _error.send(res, _error.PERMISSION_DENIED, req.user.user_role);
    }
};
