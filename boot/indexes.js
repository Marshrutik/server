var log = require('./../log');
var db = require('../libs/db');

module.exports = function (app) {
    db.updateIndexRouteParts();
    db.updateIndexUsers();

    log.info('Indexes update');
};