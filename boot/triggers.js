var log = require('./../log');
var db = require('../libs/db');

module.exports = function (app) {
    db.updateTriggerAfterApproveRouteIncrementApprovedRoutes();
    db.updateTriggerAfterDeleteRoute();
    db.updateTriggerAfterDeleteUser();

    log.info('Triggers update');
};

