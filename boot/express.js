var log = require('./../log'); // Пакет для логирования
var _error = require('../libs/error');

module.exports = function(app) {
    app.set('port', process.env.PORT || 3000);
    app.use(function(req, res){
        _error.send(res, _error.PAGE_NOT_FOUND);
    });
    app.use(function(err, req, res, next){
        _error.send(res, _error.INTERNAL_SERVER_ERROR, err.stack);
        next(err);
    })
    log.info("Express.js connect");
};