var winston = require( 'winston' );

winston.setLevels( winston.config.npm.levels );
winston.addColors( winston.config.npm.colors );

var log = new ( winston.Logger )( {
    transports: [
        new winston.transports.Console( {
            level: 'debug', // Only write logs of warn level or higher
            colorize: true
        } )
    ]
} );

module.exports = log;


// Use this singleton instance of logger like:
// logger = require( 'Logger.js' );
// logger.debug( 'your debug statement' );
// logger.warn( 'your warning' );